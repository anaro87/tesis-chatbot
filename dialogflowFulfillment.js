// See https://github.com/dialogflow/dialogflow-fulfillment-nodejs
// for Dialogflow fulfillment library docs, samples, and to report issues
// this is the file that runs for local run, server code in server.js
'use strict';

// const functions = require('firebase-functions');
const { WebhookClient } = require('dialogflow-fulfillment');
const { Card, Suggestion } = require('dialogflow-fulfillment');
const mysql = require('mysql2/promise');
const moment = require('moment');
const process = require('process');
const dotenv = require('dotenv');
dotenv.config();

process.env.DEBUG = 'dialogflow:debug'; // enables lib debugging statements

// exports.dialogflowFirebaseFulfillment = functions.https.onRequest((request, response) => {
exports.dialogflowFirebaseFulfillment = (request, response) => {
  const agent = new WebhookClient({ request, response });
  console.log('Dialogflow Request headers: ' + JSON.stringify(request.headers));
  console.log('Dialogflow Request body: ' + JSON.stringify(request.body));



  function welcome(agent) {
    agent.add(`Welcome to my agent!`);
}

function fallback(agent) {
    agent.add(`I didn't understand`);
    agent.add(`I'm sorry, can you try again?`);
}

/**
* @createdBy ana.rodriguez
* @createdAt 2022
* @Description connect to database
*/
const mysqlConnect = async ()=> {
    // public connection inside GCP
    // const con = await mysql.createConnection({
    //     socketPath: '/cloudsql/anabot-elrn:us-central1:technical-support',
    //     user: 'root',
    //     password: 'tesis#2022@-1',
    //     database: 'technical_support', 
    // });

    //public connection outside GCP
    const con = await mysql.createConnection({
        host: '34.123.88.184',
        user: 'root',
        password: 'tesis#2022@-1',
        port: '3306',
        database: 'technical_support', 
    });

    try {
        console.log("connected");
        return con;

    } catch (error) {
        console.error(`Error: ${error}`);
        return null;
    }
}

/**
* @createdBy ana.rodriguez
* @createdAt 2022
* @Description get ticket status
* @intent: ticket-status
*/  
const getStatus = async (agent) => {
    console.log(" ********** getStatus *********");
    const id = agent.parameters['ticket-id'];//200;
    const con = await mysqlConnect();
    console.log(" ********** getStatus 2 *********");
    if (con != null){
        try {
            const [rows, fields] = await con.execute('SELECT status FROM Tickets WHERE id = ?', [id]);

            if(rows == null || rows == undefined || rows == [] || rows.length == 0){
                const msg = `El ticket #${id} no existe`;
                console.log(msg); // TODO delete
                agent.add(msg);
            } else if(rows[0].status == null){
                const msg = `El ESTADO para el ticket #${id}  no fue encontrado`;
                console.log(msg); // TODO  delete
                agent.add(msg);
            }else {
                console.log(rows[0].status); // TODO delete
                const status = rows[0].status;
                agent.add(`The status for ticket #${id} is:${status}`);
            }
        } catch (error) {
            console.log(error);
        }
    }

    con.destroy
}

/**
* @createdBy ana.rodriguez
* @createdAt 2022
* @Description get ticket status
* @intent: ticket-last-update
*/
const getLastUpdate = async (agent) => {
    console.log(" ********** getLastUpdate *********");
    const id = agent.parameters['ticket-id'];//1;
    const con = await mysqlConnect();
    if (con != null){
        try {
		    const [rows, fields] = await con.execute('SELECT updated_at FROM Tickets WHERE id = ?', [id]);
            
            if(rows == null || rows == undefined || rows == [] || rows.length == 0){
			    const msg = `El ticket #${id} no existe`;
                console.log(msg); // TODO delete
                agent.add(msg);
            
            }else if(rows[0].updated_at == null){
                const msg = `No se encontro fecha de actualizacion para el ticket #${id}`;
				console.log(msg); // TODO delete
				agent.add(msg);
           
			}else {
				const dateStr = rows[0].updated_at;
				const lastUpdatedDate = moment(dateStr).format('DD-MM-YY HH:mm:ss');
				console.log(lastUpdatedDate); // TODO delete
				agent.add(`La ultima fecha de actualizacion para el ticket #${id} fue: ${lastUpdatedDate}`);

			}
        } catch (error) {
            console.log(error);
        }
    }

    con.destroy
}

/**
* @createdBy ana.rodriguez
* @createdAt 2022
* @Description get ticket status
* @intent: ticket-notes
*/
const getNotes = async (agent) => {
    console.log(" ********** getNotes *********");
    const id = agent.parameters['ticket-id'];//8;
    const con = await mysqlConnect();
    if (con != null){
        try {
            const [rows, fields] = await con.execute('SELECT notes FROM Tickets WHERE id = ?', [id]);
            console.log(rows);
            console.log(rows.length);

            if(rows == null || rows == undefined || rows == [] || rows.length == 0){
                const msg = `El ticket #${id} no existe`;
                console.log(msg); // TODO delete
                agent.add(msg);
            }else if(rows[0].notes == null){
                const msg = `No se encontraron comentarios para el ticket #${id}`;
                console.log(msg); // TODO delete
                agent.add(msg);
            }else{
                const notes = rows[0].notes;
                agent.add(`Los comentarios para el ticket #${id} son: 
                        ${notes} `
                        );
                console.log(rows[0].notes); // TODO delete
            }

        } catch (error) {
            console.log(error);
        }
    }

    con.destroy
}

/**
* @createdBy ana.rodriguez
* @createdAt 2022
* @Description get ticket status
* @intent: pet-greeting
*/
const petGreeting = async (agent) => {
    console.log(" ********** petGreeting *********");
    const name = agent.parameters['pet-name']['name'];
    console.log(`********** ${name} *********`);
        try {
            agent.add(`Hello my beautiful pet ${name} `);
        } catch (error) {
            console.log(error);
        }
}


let intentMap = new Map();
intentMap.set('Default Welcome Intent', welcome);
intentMap.set('Default Fallback Intent', fallback);
intentMap.set('ticket-status', getStatus);
intentMap.set('ticket-last-update', getLastUpdate);
intentMap.set('ticket-notes', getNotes);
intentMap.set('pet-greeting', petGreeting);
agent.handleRequest(intentMap);
};
//});
