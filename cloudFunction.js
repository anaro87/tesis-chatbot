/**
 * Copyright 2017 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

'use strict';

const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp();
const {WebhookClient} = require('dialogflow-fulfillment');
const mysql = require('mysql2/promise');
const moment = require('moment');
const process = require('process');
const dotenv = require('dotenv');
dotenv.config();
 
exports.dialogflowFirebaseFulfillment = functions.https.onRequest((request, response) => {
    const agent = new WebhookClient({ request, response });

    function welcome(agent) {
        agent.add(`Welcome to my agent!`);
    }

    function fallback(agent) {
        agent.add(`I didn't understand`);
        agent.add(`I'm sorry, can you try again?`);
    }

    
    const mysqlConnect = async ()=> {
        try {
            const con = await mysql.createConnection({
                socketPath: '/cloudsql/anabot-elrn:us-central1:technical-support',
                user: 'root',
                password: 'tesis#2022@-1',
                database: 'technical_support', 
            });

            console.log("connected");
            return con;

        } catch (error) {
            console.log(`Error: ${error}`);
            return null;
        }
    }

    
    const getStatus = async (agent) => {
        console.log(" ********** getStatus *********");
        const id = agent.parameters['ticket-id'];//200;
        const con = await mysqlConnect();
        console.log(" ********** getStatus 2 *********");
        if (con != null){
            try {
                const [rows, fields] = await con.execute('SELECT status FROM Tickets WHERE id = ?', [id]);

                if(rows == null || rows == undefined || rows == [] || rows.length == 0){
                    const msg = `El ticket #${id} no existe`;
                    console.log(msg); // TODO delete
                    agent.add(msg);
                } else if(rows[0].status == null){
                    const msg = `El ESTADO para el ticket #${id}  no fue encontrado`;
                    console.log(msg); // TODO  delete
                    agent.add(msg);
                }else {
                    console.log(rows[0].status); // TODO delete
                    const status = rows[0].status;
                    agent.add(`The status for ticket #${id} is:${status}`);
                }
            } catch (error) {
                console.log(error);
            }
        }

        con.destroy
    }

   
    const getLastUpdate = async (agent) => {
        console.log(" ********** getLastUpdate *********");
        const id = agent.parameters['ticket-id'];//1;
        const con = await mysqlConnect();
        if (con != null){
            try {
                const [rows, fields] = await con.execute('SELECT updated_at FROM Tickets WHERE id = ?', [id]);
                
                if(rows == null || rows == undefined || rows == [] || rows.length == 0){
                    const msg = `El ticket #${id} no existe`;
                    console.log(msg); // TODO delete
                    agent.add(msg);
                
                }else if(rows[0].updated_at == null){
                    const msg = `No se encontro fecha de actualizacion para el ticket #${id}`;
                    console.log(msg); // TODO delete
                    agent.add(msg);
            
                }else {
                    const dateStr = rows[0].updated_at;
                    const lastUpdatedDate = moment(dateStr).format('DD-MM-YY HH:mm:ss');
                    console.log(lastUpdatedDate); // TODO delete
                    agent.add(`La ultima fecha de actualizacion para el ticket #${id} fue: ${lastUpdatedDate}`);

                }
            } catch (error) {
                console.log(error);
            }
        }

        con.destroy
    }

   
    const getNotes = async (agent) => {
        console.log(" ********** getNotes *********");
        const id = agent.parameters['ticket-id'];//8;
        const con = await mysqlConnect();
        if (con != null){
            try {
                const [rows, fields] = await con.execute('SELECT notes FROM Tickets WHERE id = ?', [id]);
                console.log(rows);
                console.log(rows.length);

                if(rows == null || rows == undefined || rows == [] || rows.length == 0){
                    const msg = `El ticket #${id} no existe`;
                    console.log(msg); // TODO delete
                    agent.add(msg);
                }else if(rows[0].notes == null){
                    const msg = `No se encontraron comentarios para el ticket #${id}`;
                    console.log(msg); // TODO delete
                    agent.add(msg);
                }else{
                    const notes = rows[0].notes;
                    agent.add(`Los comentarios para el ticket #${id} son: 
                            ${notes} `
                            );
                    console.log(rows[0].notes); // TODO delete
                }

            } catch (error) {
                console.log(error);
            }
        }

        con.destroy
    }

   
    let intentMap = new Map();
    intentMap.set('Default Welcome Intent', welcome);
    intentMap.set('Default Fallback Intent', fallback);
    intentMap.set('ticket-status', getStatus);
    intentMap.set('ticket-last-update', getLastUpdate);
    intentMap.set('ticket-notes', getNotes);
    agent.handleRequest(intentMap);
 });

  