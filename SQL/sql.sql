-- GCP instnce technical-support
-- tesis#2022@-1


CREATE DATABASE technical_support;

DROP TABLE Tickets;

CREATE TABLE Tickets (
    id int NOT NULL AUTO_INCREMENT,
    description text,
    notes text,
    reporter varchar(150),
    contact_email varchar(150),
    contact_phone varchar(50),
    status varchar(50),
    resolver varchar(150),
    resolver_email varchar(50),
    created_at timestamp DEFAULT NOW(),
    updated_at timestamp DEFAULT NOW(),
    PRIMARY KEY (id)
);

INSERT INTO Tickets (description, notes, reporter, contact_email, contact_phone, status, resolver, resolver_email, created_at, updated_at )
VALUES (
    'I cant login to my email', 'user was inactive by inactivity, account was acitivated again', 'lolo rodriguez',
    ' lolo.test@gmail.com', ' 1-800-310-8189', 'Fixed', 'ana rodriguez', 'ana.suport@gmail.com',
    (SELECT timestamp('2022-01-01 14:53:27') - INTERVAL FLOOR( RAND( ) * 366) DAY), (SELECT timestamp('2022-01-01 14:53:27') - INTERVAL FLOOR( RAND( ) * 366) DAY) 
    );

INSERT INTO Tickets (description, notes, reporter, contact_email, contact_phone, status, resolver, resolver_email, created_at, updated_at )
VALUES (
    'No me puedo conectar a la base de datos sales_advertisers', 'usuario no tiene accesso a la base de datos, se pedira acceso al equipo de SQL', 'karen cornejo',
    'karen.test@gmail.com', '1-800-310-8189', 'In Progress', 'ana rodriguez', 'ana.suport@gmail.com',
    (SELECT timestamp('2022-01-01 14:53:27') - INTERVAL FLOOR( RAND( ) * 366) DAY), (SELECT timestamp('2022-01-01 14:53:27') - INTERVAL FLOOR( RAND( ) * 366) DAY)
 );


INSERT INTO Tickets (description, notes, reporter, contact_email, contact_phone, status, resolver, resolver_email, created_at, updated_at )
VALUES (
        'I dont have access to billing system in test environment', 'access was request to development team', 'jorge burgos',
        'jorge.test@gmail.com', '1-800-310-8170', 'in progress', 'ana rodriguez', 'ana.suport@gmail.com',
        (SELECT timestamp('2022-01-01 14:53:27') - INTERVAL FLOOR( RAND( ) * 366) DAY), (SELECT timestamp('2022-01-01 14:53:27') - INTERVAL FLOOR( RAND( ) * 366) DAY)
    );

INSERT INTO Tickets (description, notes, reporter, contact_email, contact_phone, status, resolver, resolver_email, created_at, updated_at )
VALUES (
        'No me puedo conectar a la base de datos sales_advertisers', 'ticket duplicado', 'karen cornejo',
        'karen.test@gmail.com', '1-800-310-7079', 'Cancelled', 'ana rodriguez', 'ana.suport@gmail.com',
        (SELECT timestamp('2022-01-01 14:53:27') - INTERVAL FLOOR( RAND( ) * 366) DAY), (SELECT timestamp('2022-01-01 14:53:27') - INTERVAL FLOOR( RAND( ) * 366) DAY)
    );

INSERT INTO Tickets (description, notes, reporter, contact_email, contact_phone, status, resolver, resolver_email, created_at, updated_at )
VALUES (
        'Mi monitor no enciende', 'Se le solicito al usuario traer la maquina a revision', 'Karla Ramirez',
        'karla.test@gmail.com', '1-800-310-6789', 'In Progress', 'ana rodriguez', 'ana.suport@gmail.com',
        (SELECT timestamp('2022-01-01 14:53:27') - INTERVAL FLOOR( RAND( ) * 366) DAY), (SELECT timestamp('2022-01-01 14:53:27') - INTERVAL FLOOR( RAND( ) * 366) DAY)
    );

INSERT INTO Tickets (description, notes, reporter, contact_email, contact_phone, status, resolver, resolver_email, created_at, updated_at )
VALUES (
        'QA environment is crashing for salesAdvertisers system', 'it was an outage in QA environment by deployment from 7:00 PM 8:00 PM EST, a notification was sent at company level', 'Roxana Hernriquez', 
        'roxana.test@gmail.com', '1-800-410-8189', 'Closed', 'ana rodriguez', 'ana.suport@gmail.com',
        (SELECT timestamp('2022-01-01 14:53:27') - INTERVAL FLOOR( RAND( ) * 366) DAY), (SELECT timestamp('2022-01-01 14:53:27') - INTERVAL FLOOR( RAND( ) * 366) DAY)
    );

INSERT INTO Tickets (description, notes, reporter, contact_email, contact_phone, status, resolver, resolver_email, created_at, updated_at )
VALUES (
        'prod environment is crashing for salesAdvertisers system', 'it was an outage in QA environment by deployment from 7:00 PM 8:00 PM EST, a notification was sent at company level', 'Evelyn Montalvo', 
        'evelyn.test@gmail.com', '1-800-412-8189', 'Closed', 'ana rodriguez', 'ana.suport@gmail.com',
        (SELECT timestamp('2022-01-01 14:53:27') - INTERVAL FLOOR( RAND( ) * 366) DAY), (SELECT timestamp('2022-01-01 14:53:27') - INTERVAL FLOOR( RAND( ) * 366) DAY)
    );

INSERT INTO Tickets (description, notes, reporter, contact_email, contact_phone, status, resolver, resolver_email, created_at, updated_at )
VALUES (
        'prod environment is crashing for salesAdvertisers system', null, 'Evelyn Montalvo', 
        'evelyn.test@gmail.com', '1-800-412-8189', 'created', 'ana rodriguez', 'ana.suport@gmail.com',
        (SELECT timestamp('2022-01-01 14:53:27') - INTERVAL FLOOR( RAND( ) * 366) DAY), null
    );
   
INSERT INTO Tickets (description, notes, reporter, contact_email, contact_phone, status, resolver, resolver_email, created_at, updated_at )
VALUES (
        'prod environment is crashing for salesAdvertisers system', null, 'Evelyn Montalvo', 
        'evelyn.test@gmail.com', '1-800-412-8189', null, 'ana rodriguez', 'ana.suport@gmail.com',
        (SELECT timestamp('2022-01-01 14:53:27') - INTERVAL FLOOR( RAND( ) * 366) DAY), null
    );

SELECT CURRENT_DATE - INTERVAL FLOOR(RAND() * 14) DAY;
(SELECT timestamp('2022-01-01 14:53:27') - INTERVAL FLOOR( RAND( ) * 366) DAY);


commit;

-- select ALL 
SELECT * FROM Tickets;

-- get status
SELECT status FROM Tickets WHERE id = 1;

-- get last updated
SELECT updated_at FROM Tickets WHERE id = 1; 

-- get note
SELECT notes FROM Tickets WHERE id = 1; 


INSERT INTO Tickets (description, notes, reporter, contact_email, contact_phone, status, resolver, resolver_email, created_at, updated_at )
VALUES (
    'I cant login to my email', 'user was inactive by inactivity, account was acitivated again', 'lolo rodriguez',
    ' lolo.test@gmail.com', ' 1-800-310-8189', 'Fixed', 'ana rodriguez', 'ana.suport@gmail.com',
    (SELECT timestamp('2022-01-01 14:53:27') - INTERVAL FLOOR( RAND( ) * 366) DAY), (SELECT timestamp('2022-01-01 14:53:27') - INTERVAL FLOOR( RAND( ) * 366) DAY) 
    );

INSERT INTO Tickets (description, notes, reporter, contact_email, contact_phone, status, resolver, resolver_email, created_at, updated_at )
VALUES (
    'No me puedo conectar a la base de datos sales_advertisers', 'usuario no tiene accesso a la base de datos, se pedira acceso al equipo de SQL', 'karen cornejo',
    'karen.test@gmail.com', '1-800-310-8189', 'In Progress', 'ana rodriguez', 'ana.suport@gmail.com',
    (SELECT timestamp('2022-01-01 14:53:27') - INTERVAL FLOOR( RAND( ) * 366) DAY), (SELECT timestamp('2022-01-01 14:53:27') - INTERVAL FLOOR( RAND( ) * 366) DAY)
 );

INSERT INTO Tickets (description, notes, reporter, contact_email, contact_phone, status, resolver, resolver_email, created_at, updated_at )
VALUES (
        'prod environment is crashing for salesAdvertisers system', null, 'Evelyn Montalvo', 
        'evelyn.test@gmail.com', '1-800-412-8189', null, 'ana rodriguez', 'ana.suport@gmail.com',
        (SELECT timestamp('2022-01-01 14:53:27') - INTERVAL FLOOR( RAND( ) * 366) DAY), null
    );