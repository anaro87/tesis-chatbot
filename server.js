// import { dialogflowFirebaseFulfillment } from './dialogflowFirebaseFulfillment';
// import express (after npm install express)
const express = require('express');
const { dialogflowFirebaseFulfillment } = require('./dialogflowFulfillment');

// create new express app and save it as "app"
const app = express();

// server configuration
const PORT = 9229;

app.use(express.json());

// create a route for the app
app.post('/', (req, res) => {
  // console.log(req.body);
  dialogflowFirebaseFulfillment(req, res);
  // res.send('Hello World');
});

// make the server listen to requests
app.listen(PORT, () => {
  console.log(`Server running at: http://localhost:${PORT}/`);
});
