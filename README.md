# created by ana.rodriguez
# chatbot-nodejs-sever
chatbot functionality using nodejs server instead of firebase server

# to run this project 
 - nodejs server.js
 - PORT 9229

# to expose install ngrok
- brew install ngrok
 # to expose using ngrok
 - ngrok http 9229
