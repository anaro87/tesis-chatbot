const mysql = require('mysql2/promise');
const moment = require('moment');
const process = require('process');
const dotenv = require('dotenv');
dotenv.config();

// function mysqlConnect() {
//     const con = mysql.createConnection({
//         host: process.env.HOST,
//         user: process.env.DB_USER,
//         password: process.env.DB_PWS,
//         port: process.env.DB_PORT,
//         database: process.env.DB_NAME, 
//     });

//     try {
//         con.connect();
//         console.log("connected");
//         return con;

//     } catch (error) {
//         console.error(`Error: ${error}`);
//         return null;
//     }
    
// }
async function mysqlConnect() {
    const con = mysql.createConnection({
        host: '34.123.88.184',
        user: 'root',
        password: 'tesis#2022@-1',
        port: '3306',
        database: 'technical_support', 
    });

    try {
        console.log("connected");
        return con;

    } catch (error) {
        console.error(`Error: ${error}`);
        return null;
    }
}

const getStatus = async (agent) => {
    console.log(" ********** getStatus *********");
    const id = 1;//agent.parameters['ticket-id'];//200;
    const con = await mysqlConnect();
    console.log(" ********** getStatus 2 *********");
    if (con != null){
        try {
          const [rows, fields] = await con.execute('SELECT status FROM Tickets WHERE id = ?', [id]);
          console.log(rows);

          if(rows == null || rows == undefined){
          	const msg = `El registro para el ticket #${id}  no fue encontrado`;
            console.log(msg); // TODO  delete
            // agent.add(msg);
                    
          }else {
            console.log(rows[0].status); // TODO delete
            const status = rows[0].status;
            // agent.add(`The status for ticket #${id} is:${status}`);
          }
            // con.query({
            //     sql: 'SELECT status FROM Tickets WHERE id = ?',
            //     timeout: 40000, // 40s
            //     values: id
            // }, function (error, results, fields) {
            //     console.log(results);
            //     if(results == null || results == undefined){
            //         const msg = `El registro para el ticket #${id}  no fue encontrado`;
            //         console.log(msg); // TODO  delete
            //         agent.add(msg);
                    
            //     }else {
            //         console.log(results[0].status); // TODO delete
            //         const status = results[0].status;
            //         agent.add(`The status for ticket #${id} is:${status}`);
            //     }
            // });
            
        } catch (error) {
            console.log(error);
        }
       
    }

    con.destroy
}


async function getLastUpdate() {
    const id = 1;
    const con = mysqlConnect();
    if (con != null){
        con.query({
            sql: 'SELECT notes FROM Tickets WHERE id = ?',
            timeout: 400000, // 4m
            values: id
          }, function (error, results, fields) {
            if(results == null || results == undefined){
                const msg = `No se encontraron actualizaciones para el ticket #${id}`;
                console.log(msg);
            }else {
              const dateStr = results[0].updated_at;
              const lastUpdatedDate = moment(dateStr).format('DD-MM-YY HH:mm:ss');
              console.log(lastUpdatedDate);
            }

            return 0;
          });

    }

    con.destroy
}

async function getNotes() {
    const id = 8;
    const con = mysqlConnect();
    if (con != null){
        con.query({
            sql: 'SELECT notes FROM Tickets WHERE id = ?',
            timeout: 40000, // 40s
            values: id
        }, function (error, results, fields) {
            if(results == null || results == undefined){
                const msg = `No se encontraron comentarios para el ticket #${id}`;
                console.log(msg);
            }else{
                console.log(results[0].notes);
            }
        });
        return 0;

    }

    con.destroy
}

// mysqlConnect();
getStatus();
// getLastUpdate();
// getNotes();