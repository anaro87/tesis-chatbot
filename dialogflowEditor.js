/**
 * Copyright 2017 Google Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

 'use strict';

 const functions = require('firebase-functions');
 const admin = require('firebase-admin');
 admin.initializeApp();
 const {google} = require('googleapis');
 const {WebhookClient} = require('dialogflow-fulfillment');
 //connect to database
 const Sequelize = require('sequelize');
 const path =  'postgres://iofge:iofge@192.168.20.17:30141/iofge'; //'postgres://user:pass@example.com:5432/dbname'
 const sequelizeCon  = new Sequelize(path);
 // const conDB  = new Sequelize('postgres://user:pass@example.com:5432/dbname');
 
 // Enter your calendar ID below and service account JSON below
 const calendarId = "6df57ba5iqaepkl53janlj4nrk@group.calendar.google.com";
 const serviceAccount = {
  "type": "service_account",
  "project_id": "anabot-elrn",
  "private_key_id": "63e41a478a40d294f7d42f0e17c4bad0aee4f3b2",
  "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCwclafsHNaJdYB\nN0BYaluRbX/cj4h7FlcRdsFuC05I1ryeCZV4viK3tJ6v3BgOQUqibyi7jAojlpUW\nj/vsgv3HSMANTvYpVkECchWadF14p2U/O42hOWLKcF0DAFj64OIu4PwYfaNqZ9gh\nIe8VNMcZLufWCcvdlF4zyuvHK2dD+NbmEgp8LNzFdOawO9FHFDX6MS32+BoumOWG\nz5R65h1WF8efODSSpRFgQIr1KvUx0tOSi1FX7V24CFR8TrkdyoleyyPeBGP+Zn3u\nFO0hISgJqBJw5ux4dvgfC3JqO7S3gFh2cm1sPpdMuGr5SgXm8FoHZKXeQ94ujaNh\nhG0rTBIbAgMBAAECggEAAuAaIUgS2Sk8r5Bi8A3rqBcMul9sbfUTPimdRVnR0MYP\nFNQ7J1iR4ESFQYBos/LEcJk3JLdHgwT1MQTvqg8Ia7L3zj+3Kz1Ime2aBIZY75pq\nv/kMedAhmAL0wRPEX3JCvVjVQuDpRNakYwXWi5XsmLB2/rnyJI5Car2n0fsXF7hz\napdL2z6w/ht9T72Mj2ln6gTPzd/J86UlMSNEOrsVEJJSFkVCdN1E4ymITLVwPA4O\nqUCta+rV2cI03u/G/gqZl00VYr4iWWOPNLQUqgJN0FEymZU/J6itxBDUCaryeRtY\nMaFcWenJqE1BCA8zTpWQhCcI+DX6ayp/LNIv34eKgQKBgQDzO6pmIb4ih+gcdxCW\nko4MGNn4AJEgBQaujK2BE5FASufkZmPWs0a795EcmRKuD08bsjopWNMmK81bfhTG\n+ALRCeFnPxBIHKzYDNsU/C+Q27F5y2iKY5Y+MDkkXyafJProHXuokCIq8xnL6oiw\nEd5LnJlPKM06hQk5qKpmnhF9CwKBgQC5tUKe8zrlMe3Ep7REEBkFlWveEVijDBCT\nbv87Mc24sTUAzZm0nUbnCxn8rxgNP7AhgoNU6ufvfssfEjSwg1xsIXSZL9kAMNkl\nVl7o5t50/kKZ5cUoVh2/gdroxVVKSyJe2v0BcDZp7n6pSiUz2LJkTDhfhNEYXJf8\nlQys+MFJMQKBgDA/faWUu+qNmvVKiPfVSmgL5fDT6/W2CYVGUoRwJTfjo+wig1tr\nZHSUvyw1EQeF8eNEj1T0Ua4Ysjub3vM47xYYJ6YbVFd5mK+inTKkc6sAaUwHlU0e\nRAdB+ZPFbBvVcg/pCUZ+kMiEQHG6rDKA2N+WyLJzHYKmMRZG5sZERKPRAoGAEKC2\nMl087MCntW/+qAl49BMI8X1EHnU6FrldiQNQlDTptgSyuoVKj+1iiklzMdx9/MIe\n2D+gPMVxknqi1Rst71CfjAfIukelab5uCRSC3p7Vf8oe+Rmc2ETcQPNKS7imfy/E\nb5iIhrugA7Br3FSOxIQ+l+7gefnwceE2nVY4mEECgYEA6qpyjPz6ySabFRkd18f0\nDYd/2ERQ3xIlFiq7UOh2uIApdlSIwhqhDeuDFwZ+l9fWLHd7+zCQ7eMTcuuM7zkG\nMMPhgcQmflUVtYqwf51o01dmRd8ZcW+I1k32iOcpXLYorPYkqFPHMiWZrKv+kx/K\nTyJdU6xJfap3CrBJ6eRdHUw=\n-----END PRIVATE KEY-----\n",
  "client_email": "appointment-scheduler@anabot-elrn.iam.gserviceaccount.com",
  "client_id": "117421575540031214828",
  "auth_uri": "https://accounts.google.com/o/oauth2/auth",
  "token_uri": "https://oauth2.googleapis.com/token",
  "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
  "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/appointment-scheduler%40anabot-elrn.iam.gserviceaccount.com"
}; // Starts with {"type": "service_account",...
 
 // Set up Google Calendar Service account credentials
 const serviceAccountAuth = new google.auth.JWT({
   email: serviceAccount.client_email,
   key: serviceAccount.private_key,
   scopes: 'https://www.googleapis.com/auth/calendar'
 });
 
 const calendar = google.calendar('v3');
 process.env.DEBUG = 'dialogflow:*'; // enables lib debugging statements
 
 const timeZone = 'America/Los_Angeles';
 const timeZoneOffset = '-07:00';
 
 exports.dialogflowFirebaseFulfillment = functions.https.onRequest((request, response) => {
   const agent = new WebhookClient({ request, response });
   console.log("Parameters", agent.parameters);
   const appointment_type = agent.parameters.appointmentType 
   function makeAppointment (agent) {
     // Calculate appointment start and end datetimes (end = +1hr from start)
     console.log("Parameters", agent.parameters.date);
     const dateTimeStart = new Date(Date.parse(agent.parameters.date.split('T')[0] + 'T' + agent.parameters.time.split('T')[1].split('-')[0] + timeZoneOffset));
     console.log(dateTimeStart);
     const dateTimeEnd = new Date(new Date(dateTimeStart).setHours(dateTimeStart.getHours() + 1));
     const appointmentTimeString = dateTimeStart.toLocaleString(
       'en-US',
       { month: 'long', day: 'numeric', hour: 'numeric', timeZone: timeZone }
     );
 
     // Check the availibility of the time, and make an appointment if there is time on the calendar
     return createCalendarEvent(dateTimeStart, dateTimeEnd, appointment_type).then(() => {
       agent.add(`Ok, let me see if we can fit you in. ${appointmentTimeString} is fine!.`);
     }).catch(() => {
       agent.add(`I'm sorry, there are no slots available for ${appointmentTimeString}.`);
     });
   }

  function getCurDate(agent){
      const today = new Date();
      const dd =  today.getDate() < 10 ? "0"+today.getDate() : today.getDate();
      const mm =  today.getMonth() + 1 < 10 ? "0"+today.getMonth() : today.getMonth();
      const yyyy = today.getFullYear();
      const curDate = yyyy+ "-"+ mm + "-"+ dd;
      console.log("TEST: " + curDate);
      agent.add(`today is ${curDate}`);
   }

	async function getITSS(agent){
      const itsskey = agent.parameters.itssNumber;
      console.log(`itss key is: ${itsskey}`);
      
      try {
        await sequelizeCon.authenticate();
        console.log('Connection has been established successfully.');
      } catch (error) {
        console.error('Unable to connect to the database:', error);
      }

      agent.add(`itss info ${itsskey}`);
    }
 
   let intentMap = new Map();
   intentMap.set('shedule-appointment', makeAppointment); //intent name shedule-appointment
   intentMap.set('ask-date', getCurDate); //intent name ask-date
   intentMap.set('get-itss', getITSS); //intent name ask-date
   agent.handleRequest(intentMap);
 });

  

 function createCalendarEvent (dateTimeStart, dateTimeEnd, appointment_type) {
   return new Promise((resolve, reject) => {
     calendar.events.list({
       auth: serviceAccountAuth, // List events for time period
       calendarId: calendarId,
       timeMin: dateTimeStart.toISOString(),
       timeMax: dateTimeEnd.toISOString()
     }, (err, calendarResponse) => {
       // Check if there is a event already on the Calendar
       if (err || calendarResponse.data.items.length > 0) {
         reject(err || new Error('Requested time conflicts with another appointment'));
       } else {
         // Create event for the requested time period
         calendar.events.insert({ auth: serviceAccountAuth,
           calendarId: calendarId,
           resource: {summary: appointment_type +' Appointment', description: appointment_type,
             start: {dateTime: dateTimeStart},
             end: {dateTime: dateTimeEnd}}
         }, (err, event) => {
           err ? reject(err) : resolve(event);
         }
         );
       }
     });
   });
 }